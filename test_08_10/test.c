#include<stdio.h>
#include<string.h>
#include<assert.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i));
//	}
//	return 0;
//}
//void sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int flag = 1;
//		int j = 0;
//		for (j = 0; j < sz - i - 1; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{ 
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,5,6,3,7,4,9,8,2,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	sort(arr, sz);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//int main()
//{
//	int a[] = { 1,2,8,9,4,10,3,5,6,0,7 };
//	int left = 0;
//	int sz = sizeof(a) / sizeof(a[0]);
//	int right = sz - 1;
//	int tmp = 0;//临时中间变量
//	int num = sz;//判断次数
//	while (num)
//	{
//		if (a[left] % 2 == 0)//此时a[left]是偶数，则进行移位
//		{
//			//ou = i;//下一次判断的起始位置
//			tmp = a[left];
//			for (int j = left; j <= right - 1; j++)
//			{
//				a[j] = a[j + 1];
//			}
//			a[right] = tmp; //偶数放在数组末尾
//		}
//		else
//		{
//			left++;//a[left]不是偶数，则判断下一个a[left+1]
//		}
//		num--;
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", a[i]);
//	}
//	return 0;
//}

int Rotate(char* str1, char* str2)
{
	assert(str1 && str2);
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	if (len1 != len2)
		return 0;
	else
	{
		while (len1--)
		{
			char temp = str1[0];
			int i = 0;
			for (; i < len1; i++)
			{
				str1[i] = str1[i + 1];
			}
			str1[i] = temp;
			if (strcmp(str1, str2) == 0)
				return 1;
		}
		return 0;
	}

}
int main()
{
	char str1[] = "ABCDEF";
	char str2[] = "DAABC";
	int a = Rotate(str1, str2);
	if (a == 1)
		printf("%s是%s旋转得到的结果", str1, str2);
	else
		printf("%s不是%s旋转得到的结果", str1, str2);
	return 0;
}

